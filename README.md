# IoTMonitor installation on Raspberry Pi OS

Developed with Eclipse IDE

## Install Java and javafx

    sudo apt-get install default-jdk

Library in /usr/lib/jvm

    sudo apt-get install openjfx

Library in /usr/share/openjfx/lib

## Get code

    git clone https://gitlab.com/ljk-java/iotmonitor

Download JSON library from:

	https://mvnrepository.com/artifact/org.json/json (Mar 2021)

Copy jar file to ~/Documents/lib



## Compiling

	javac -cp /usr/share/openjfx/lib/*:/home/pi/Documents/lib/json-20210307.jar:.  org/iot_lab/iot/IoTmonitor.java

## Run

	java -cp /usr/share/openjfx/lib/*:/home/pi/Documents/lib/json-20210307.jar:. --module-path /usr/share/openjfx/lib --add-modules=javafx.controls org/iot_lab/iot/IoTmonitor.java


