package org.iot_lab.iot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.iot_lab.iot.controller.APIcontroller;
import org.iot_lab.iot.controller.HTTPservice;
import org.iot_lab.iot.model.Node;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class IoTmonitor extends Application
{
    private static HTTPservice httpService;
    private static ArrayList<Node> nodeList;
    private static Timer timer;
    private static Calendar calendar;
    
    
    @Override
    public void start(Stage stage) throws Exception
    {
	GridPane root = new GridPane();
	Scene scene = new Scene(root, 730, 508);
	
	nodeList = new ArrayList<>();
	
	// read all nodes from API
	APIcontroller api = new APIcontroller();
	
	api.getNodes(nodeList);
	
	// hand them over to MonitorGui
	new MonitorGui(root, nodeList);
	
	httpService = new HTTPservice(nodeList);
	
	stage.setTitle("IoT monitor");
	stage.setAlwaysOnTop(true);
	stage.setScene(scene);
	stage.show();
	
	timer = new Timer(true);
	
	// Get the Date corresponding to 11:59:59 pm today.
	calendar = Calendar.getInstance();
	calendar.set(Calendar.HOUR_OF_DAY, 23);
	calendar.set(Calendar.MINUTE, 59);
	calendar.set(Calendar.SECOND, 59);
	calendar.set(Calendar.MILLISECOND, 990);
	Date time = calendar.getTime();
	
	timer.schedule(new MyTimerTask(), time);
    }
    
    public static void main(String[] args)
    {
	launch(args);
	System.out.println("application stopped");
	httpService.stop();
    }
    
    
    private class MyTimerTask extends TimerTask
    {
	@Override
	public void run()
	{    
	    nodeList.forEach(node -> node.resetDayCounter());
	    
	    // and schedule another one
	    calendar = Calendar.getInstance();
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 990);
	    Date time = calendar.getTime();
		
	    timer.schedule(new MyTimerTask(), time);
	}
	
	
    }

    
}
