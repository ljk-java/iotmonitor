package org.iot_lab.iot;

import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;

import org.iot_lab.iot.model.*;

public class MonitorGui
{
    Button btn;
    ArrayList<NodeGui> nodeGuiList;
    
    NodeGui node, node2;
    
    static int x = 0;
    static int y = 0;
    static final int ROWS = 5;
    
    public MonitorGui(GridPane pane, ArrayList<Node> nodeList)
    {
	
	
	nodeGuiList = new ArrayList<>();
	
	pane.setVgap(1);
	pane.setHgap(1);
	pane.setPadding(new Insets(1,1,1,1));
	
	nodeList.forEach(node ->{
	    NodeGui nodegui = new NodeGui(node);
	    nodeGuiList.add(nodegui);
	});
	
	nodeGuiList.stream().forEach(nodeGui -> 
	{
	    if ((y % ROWS) == 0) x++; 
	    y = y - (y/ROWS)*ROWS;  
	    pane.add(nodeGui, x, y);
	    y++;    
	});
    }    
    
}
