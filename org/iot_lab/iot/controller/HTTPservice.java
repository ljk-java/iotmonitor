package org.iot_lab.iot.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.ArrayList;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import org.iot_lab.iot.model.*;

public class HTTPservice
{
    HttpServer server;
    ArrayList<Node> nodeList;
    
    
    public HTTPservice(ArrayList<Node> nodeList)
    {
	this.nodeList = nodeList;
	
	try 
	{
	    server = HttpServer.create(new InetSocketAddress(8000), 0);
	    server.createContext("/reset", new MyHandler());
	    server.setExecutor(null);
	    server.start();
	    System.out.println("HTTP server started!");
	}
	catch(IOException ex)
	{
	    System.out.println("could not start HTTP server");
	}
	
    }
    

    public void stop()
    {
	server.stop(0);
	while (server!=null) {}    
    }
    
    
    private class MyHandler implements HttpHandler
    {
	@Override
	public void handle(HttpExchange httpEx) throws IOException
	{
	    // get
	    String uriStr = httpEx.getRequestURI().toString();
	    String[] uriArray = uriStr.split("/");
	    try
	    {
		int id = Integer.parseInt(uriArray[uriArray.length-1]);
		System.out.println("Id: " + id);
		Node node = nodeList.stream().filter(p -> (p.getId() == id)).findFirst().get();
		node.resetTimer();
		node.incrDayCounter();
	    }
	    catch(NumberFormatException ex)
	    {
		System.err.println("Error api input");
	    }
	    // response
	    String response = "OK 200\n";
	    httpEx.sendResponseHeaders(200, response.length());
	    OutputStream os = httpEx.getResponseBody();
	    os.write(response.getBytes());
	    os.close();
	}
    }
    
}
